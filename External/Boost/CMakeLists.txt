#
# Configuration for building Boost as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( Boost )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Add an option for forcing the build of Boost:
option( BOOST_FORCE_BUILD
   "Force building Boost as part of the release, even if an appropriate version is found on the system"
   OFF )

# Look for Boost. We generally need at least 1.58.
find_package( Boost 1.58 )
if( Boost_FOUND AND NOT BOOST_FORCE_BUILD )
   message( STATUS "Boost ${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}"
      ".${Boost_SUBMINOR_VERSION} found under:" )
   message( STATUS "   ${Boost_INCLUDE_DIR}" )
   message( STATUS "Not building it as part of this project" )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Boost as part of this project" )

# The source code of Boost:
set( _boostSource
   "http://cern.ch/service-spi/external/tarFiles/boost_1_59_0.tar.gz" )
set( _boostMd5 "51528a0e3b33d9e10aaa311d9eb451e3" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/BoostBuild )

# Use all available cores for the build:
atlas_cpu_cores( nCPUs )

# The build needs Python. If 2.7 is not available on the system, then we
# must be building it ourselves.
find_package( PythonInterp 2.7 )
if( PYTHONINTERP_FOUND )
   set( _extraConf --with-python=${PYTHON_EXECUTABLE} )
else()
   set( _extraConf --with-python=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python )
endif()

# Only add debug symbols in Debug build mode:
set( _extraOpt )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   set( _extraOpt "variant=debug" )
endif()

# Build Boost:
ExternalProject_Add( Boost
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_boostSource}
   URL_MD5 ${_boostMd5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ./bootstrap.sh --prefix=${_buildDir} ${_extraConf}
   BUILD_COMMAND ./b2 ${_extraOpt} -j${nCPUs}
   COMMAND ./b2 ${_extraOpt} install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/
   <INSTALL_DIR> )
add_dependencies( Package_Boost Boost )

# If we are building Python ourselves, make Boost depend on it:
if( NOT PYTHONINTERP_FOUND )
   add_dependencies( Boost Python )
endif()

# Install Boost:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
