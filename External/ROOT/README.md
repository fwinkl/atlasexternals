ROOT - Data Analysis Framework
==============================

This package is used to build ROOT for the offline / analysis release.

If an appropriate version of ROOT is already found on the system, then the
package doesn't do anything. Unless you force it to build ROOT no matter what,
using the

    -DROOT_FORCE_BUILD:BOOL=TRUE

CMake configuration option.
